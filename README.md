# Фабрика Решений - тестовое задание


### Задание
См. [issue](https://gitlab.com/vlad_397/fabrique_v_test/-/issues/1)

Выполненные доп. пункты: 3, 5, 6


### Описание

`Swagger` документация доступна по адресу *http://127.0.0.1/docs/*

`Админка` доступна по адресу *http://127.0.0.1/admin/*


Возможности проекта:

- добавление, удаление, изменение клиента;
- добавление, удаление, изменение рассылки;
- получение общей статистики рассылок;
- получение статистики конкретной рассылки.


### Подробности выполнения

Модели: `Client`, `Mailing`, `Message`.

Поля модели `Client`:
- phone_number - номер телефона;
- phone_code - код оператора;
- tag - тег;
- timezone_number - часовой пояс.

Поля модели `Mailing`:
- name;
- start_datetime - дата и время запуска рассылки;
- text - текст рассылки;
- filter - фильтр клиентов по оператору или тегу;
- stop_datetime - дата и время конца рассылки.

Поля модели `Message`:
- created_at;
- status;
- mailing_id - fk на модель `Mailing`;
- client_id - fk на модель `Client`.

После создания рассылки клиенты фильтруются по указанному в рассылке полю `filter` и создаются объекты модели `Message` 
со статусом *Создано*. Если текующее время находится между началом и концом, то рассылка происходит немедленно. Если
текущее время меньше, чем время начала, то рассылка будет выполнена в назначенный срок с помощью `Celery + RabbitMQ`.
При этом будет определен статус *"Отправлено"*, *"Неудачная отправка. Пытаемся снова"* или 
*"Не удалось отправить спустя 2 попытки. Попыток больше не будет."*. В противном случае рассылка выполнена не будет 
со статусом *"Не было отправлено вовремя"*.

Сама отправка сообщений происходит через предоставленное внешнее API. После отправки проверяется статус код ответа.
Если статус код равен 200, то сообщение помечается как *"Отправлено"*. В противном случае будет отметка 
*"Неудачная отправка. Пытаемся снова"*. Далее будет произведена еще одна попытка для каждого из пользователей, кому не
удалось отправить сообщение. Если попытка вновь неудачная, то статус сообщения изменится на 
*"Не удалось отправить спустя 2 попытки. Попыток больше не будет."*.

Помимо эндпоинтов статистики можно воспользоваться панелью администратора, где объекты модели `Message` могут быть
отфильтрованы по рассылке и по статусу. Более того, в панели администратора внутри каждого объекта модели `Mailing`
указаны все связанные объекты модели `Message` с помощью *admin.TabularInline*.


### Запуск проекта
После клонирования репозитария создайте `.env` файл на уровне с `env_sample.txt` - шаблоном env-файла

Для установки Docker используйте команду `sudo apt install docker docker-compose`

Далее выполните следующие команды:

`sudo docker-compose up -d --build` *Для запуска сборки контейнеров*

`sudo docker-compose exec backend python manage.py makemigrations` *Для создания миграций*

`sudo docker-compose exec backend python manage.py migrate` *Для применения миграций*

`sudo docker-compose exec backend python manage.py createsuperuser` *Для создания суперпользователя*

`sudo docker-compose exec backend python manage.py collectstatic` *Для сбора статических файлов*
