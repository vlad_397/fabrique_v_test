from django.core.validators import RegexValidator
from django.db import models

TAGS = [
    ('Тег1', 'тег1'),
    ('Тег2', 'тег2'),
    ('Тег3', 'тег3'),
]


class Client(models.Model):
    phone_regex = RegexValidator(
        regex=r'^[0-9]{11}$', message="Номер телефона должен быть в формате: '7XXXXXXXXXX', где X - цифра от 0 до 9"
    )
    phone_number = models.CharField(
        validators=[phone_regex], max_length=11, verbose_name='Номер телефона', unique=True
    )
    phone_code_regex = RegexValidator(regex=r'^[0-9]{3}$', message='Код должен содержать три цифры')
    phone_code = models.CharField(
        validators=[phone_code_regex], max_length=3, verbose_name='Код оператора'
    )
    tag = models.CharField(max_length=150, choices=TAGS, verbose_name='Тег')
    timezone_regex = RegexValidator(
        regex=r'^UTC[-+][0-9]{1,2}$', message="Часовой пояс должен быть в формате UTC±час"
    )
    timezone_number = models.CharField(
        validators=[timezone_regex], max_length=6, verbose_name='Часовой пояс'
    )

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'

    def __str__(self):
        return f'{self.phone_number} (id={self.id})'


class Mailing(models.Model):
    name = models.CharField(
        max_length=150, verbose_name='Название рассылки', unique=True,
        error_messages={'unique': ('Mailing with that name already exists.')}
    )
    start_datetime = models.DateTimeField(verbose_name='Дата и время запуска рассылки')
    text = models.TextField(verbose_name='Текст рассылки')
    filter = models.CharField(max_length=150, verbose_name='Фильтр свойств клиента', )
    stop_datetime = models.DateTimeField(verbose_name='Дата и время окончания рассылки')

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'

    def __str__(self):
        return f'{self.name} (id={self.id})'


class Message(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата и время создания')
    status = models.CharField(verbose_name='Статус отправки', max_length=150, default='Создано')
    mailing_id = models.ForeignKey(
        Mailing, on_delete=models.CASCADE, related_name='message',
        verbose_name='Рассылка'
    )
    client_id = models.ForeignKey(
        Client, on_delete=models.CASCADE, related_name='message',
        verbose_name='Клиент'
    )

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'

    def __str__(self):
        return f'{self.id}'
