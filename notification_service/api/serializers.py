from rest_framework import serializers

from .models import Client, Mailing


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'


class MailingSerializer(serializers.ModelSerializer):
    start_datetime = serializers.DateTimeField(format='%d-%m-%Y %H:%M:%S')
    stop_datetime = serializers.DateTimeField(format='%d-%m-%Y %H:%M:%S')

    class Meta:
        model = Mailing
        fields = '__all__'
