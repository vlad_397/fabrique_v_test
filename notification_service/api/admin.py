from django.contrib import admin

from .models import Client, Mailing, Message


class MessageInline(admin.TabularInline):
    model = Message

    def has_change_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class MessageAdmin(admin.ModelAdmin):
    readonly_fields = ('status',)
    list_display = ('id', 'mailing_id', 'client_id', 'status',)
    list_filter = ('status', 'mailing_id')


class MailingAdmin(admin.ModelAdmin):
    inlines = (MessageInline,)

    def get_form(self, request, obj=None, **kwargs):
        form = super(MailingAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['filter'].help_text = 'Код мобильного оператора или тег'
        return form


admin.site.register(Client)
admin.site.register(Mailing, MailingAdmin)
admin.site.register(Message, MessageAdmin)
