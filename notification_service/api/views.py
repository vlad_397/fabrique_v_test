import datetime
import logging

from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from drf_yasg.utils import swagger_auto_schema
from rest_framework import mixins, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from .docs import (client_ok_responses, client_responses, client_schema,
                   get_overall_statistics_response, get_statistics_response,
                   mailing_ok_responses, mailing_responses, mailing_schema)
from .models import Client, Mailing, Message
from .serializers import ClientSerializer, MailingSerializer
from .tasks import prepare_mailing

logger = logging.getLogger(__name__)


class CreateUpdateDeleteViewSet(mixins.CreateModelMixin, mixins.UpdateModelMixin, mixins.DestroyModelMixin,
                                viewsets.GenericViewSet):
    pass


@method_decorator(name='create', decorator=swagger_auto_schema(
    request_body=client_schema, responses=client_responses
))
@method_decorator(name='update', decorator=swagger_auto_schema(
    request_body=client_schema, responses=client_ok_responses
))
@method_decorator(name='partial_update', decorator=swagger_auto_schema(
    request_body=client_schema, responses=client_ok_responses
))
class ClientViewSet(CreateUpdateDeleteViewSet):
    """Класс, отвечающий за работу с клиентами"""
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


@method_decorator(name='update', decorator=swagger_auto_schema(
    request_body=mailing_schema, responses=mailing_ok_responses
))
@method_decorator(name='partial_update', decorator=swagger_auto_schema(
    request_body=mailing_schema, responses=mailing_ok_responses
))
class MailingViewSet(CreateUpdateDeleteViewSet):
    """Класс, отвечающий за работу с рассылками"""
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer

    @swagger_auto_schema(request_body=mailing_schema, responses=mailing_responses)
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        now = datetime.datetime.utcnow()
        start_datetime = datetime.datetime.strptime(request.data['start_datetime'], '%d-%m-%Y %H:%M:%S')

        if now >= start_datetime:
            prepare_mailing.delay(serializer.data)
        else:
            prepare_mailing.apply_async(args=[serializer.data], eta=start_datetime)

        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    @swagger_auto_schema(responses=get_overall_statistics_response)
    @action(methods=['get'], detail=False)
    def get_overall_statistics(self, request, *args, **kwargs):
        # Функция получения общей статистики рассылок
        mailings = Mailing.objects.all()
        result = {}
        for mailing in mailings:
            sent_messages = Message.objects.filter(mailing_id=mailing, status='Отправлено').count()
            unsent_messages = Message.objects.filter(~Q(status='Отправлено'), mailing_id=mailing).count()
            result[f'{mailing.name} (id={mailing.id})'] = {'Отправлено': sent_messages,
                                                           'Не отправлено': unsent_messages}

        return Response(result, status=status.HTTP_200_OK)

    @swagger_auto_schema(responses=get_statistics_response)
    @action(methods=['get'], detail=True)
    def get_statistics(self, request, *args, **kwargs):
        # Функция получения статистики конкретной рассылки
        mailing = get_object_or_404(Mailing, id=int(kwargs['pk']))
        result = {}

        sent_messages = Message.objects.filter(mailing_id=mailing, status='Отправлено')
        unsent_messages = Message.objects.filter(~Q(status='Отправлено'), mailing_id=mailing)

        sent_count = sent_messages.count()
        unsent_count = unsent_messages.count()
        result[f'{mailing.name} (id={mailing.id})'] = {'Отправлено': sent_count, 'Не отправлено': unsent_count}
        result['Отправленные сообщения'] = [
            {'id сообщения': msg.id,
             'id клиента': msg.client_id.id,
             'телефон': msg.client_id.phone_number} for msg in sent_messages
        ]
        result['Не отправленные сообщения'] = [
            {'id сообщения': msg.id,
             'id клиента': msg.client_id.id,
             'телефон': msg.client_id.phone_number} for msg in unsent_messages
        ]

        return Response(result, status=status.HTTP_200_OK)
