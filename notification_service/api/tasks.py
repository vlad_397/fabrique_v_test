import datetime
import logging

import requests
from celery import shared_task

from notification_service.settings import SENDING_API, SENDING_API_TOKEN

from .models import Client, Mailing, Message

logger = logging.getLogger(__name__)


@shared_task
def prepare_mailing(data: dict) -> bool:
    """Функция подготовки списка клиентов"""
    json_clients = []

    start_datetime = datetime.datetime.strptime(data['start_datetime'], '%d-%m-%Y %H:%M:%S')
    stop_datetime = datetime.datetime.strptime(data['stop_datetime'], '%d-%m-%Y %H:%M:%S')

    filter_param = data['filter']
    if len(filter_param) == 3:
        clients = Client.objects.filter(phone_code=filter_param)
    else:
        clients = Client.objects.filter(tag=filter_param)

    mailing = Mailing.objects.get(id=data['id'])

    for client in clients:
        message = Message.objects.create(mailing_id=mailing, client_id=client)
        logger.info(f'Message (id={message.id}) created')

        json_client = {'id': message.id, 'phone': int(client.phone_number), 'text': mailing.text, 'message': message}

        json_clients.append(json_client)
        logger.info(f'Message (id={message.id}) was added to sending list')

    unsuccess_clients = send_mailing(json_clients, start_datetime, stop_datetime)
    if len(unsuccess_clients) > 0:
        unsuccess_clients_2 = send_mailing(unsuccess_clients, start_datetime, stop_datetime)
    else:
        unsuccess_clients_2 = []

    if len(unsuccess_clients_2) > 0:
        for unsuccess_client in unsuccess_clients_2:
            message = unsuccess_client['message']
            message.status = 'Не удалось отправить спустя 2 попытки. Попыток больше не будет.'
            message.save()
            logger.info(f'Message (id={message.id}) unsuccess after 2 tries. No more tries')

    return True


def send_mailing(data: list, start: datetime.datetime, stop: datetime.datetime) -> list:
    """Функция отправки на внешний API"""
    unsuccess_json_clients = []

    now = datetime.datetime.utcnow()

    for client in data:
        message = client.pop('message')
        message_id = client['id']

        if stop > now >= start:
            try:
                response = requests.post(
                    url=f'{SENDING_API}{message_id}',
                    json=client,
                    headers={'Authorization': f'Bearer {SENDING_API_TOKEN}'}
                )

                if response.status_code == 200:
                    message.status = 'Отправлено'
                    logger.info(f'Message (id={message.id}) sent successfully')
                else:
                    message.status = 'Неудачная отправка. Пытаемся снова'
                    logger.info(f'Message (id={message.id}) sent unsuccessfully, retry')
                    client['message'] = message
                    if client not in unsuccess_json_clients:
                        unsuccess_json_clients.append(client)
                message.save()

            except Exception as e:
                message.status = 'Неудачная отправка. Пытаемся снова'
                message.save()
                logger.info(f'Message (id={message.id}) sent unsuccessfully, Exception: {e}, retry')
                client['message'] = message
                if client not in unsuccess_json_clients:
                    unsuccess_json_clients.append(client)

        else:
            message.status = 'Не было отправлено вовремя'
            message.save()
            logger.info(f'Message (id={message.id}) sending time is over')

    return unsuccess_json_clients
