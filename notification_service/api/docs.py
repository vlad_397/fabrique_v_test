from django.utils.translation import ugettext_lazy as _
from drf_yasg import openapi

client_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'phone_number': openapi.Schema(
            type=openapi.TYPE_STRING, description=_('Номер телефона'), example='79999999999'
        ),
        'phone_code': openapi.Schema(type=openapi.TYPE_STRING, description=_('Код оператора'), example='999'),
        'tag': openapi.Schema(type=openapi.TYPE_STRING, description=_('Тег'), example="Тег1"),
        'timezone_number': openapi.Schema(
            type=openapi.TYPE_STRING, description=_('Часовой пояс'), example=_("UTC+7")
        ),
    }
)

client_responses = {
    "201": openapi.Response(
        description="Создано",
        examples={
            "application/json": {
                "id": 1,
                "phone_number": "79999999999",
                "phone_code": "999",
                "tag": "Тег1",
                "timezone_number": "UTC+7"
            }
        }
    ),
    "400": openapi.Response(
        description="Ошибка",
        examples={
            "application/json": {
                "phone_number": [
                    "Номер телефона должен быть в формате: '7XXXXXXXXXX', где X - цифра от 0 до 9"
                ]
            }
        }
    ),
}

client_ok_responses = {
    "200": openapi.Response(
        description="ОК",
        examples={
            "application/json": {
                "id": 1,
                "phone_number": "79999999999",
                "phone_code": "999",
                "tag": "Тег1",
                "timezone_number": "UTC+7"
            }
        }
    ),
    "400": openapi.Response(
        description="Ошибка",
        examples={
            "application/json": {
                "phone_number": [
                    "Номер телефона должен быть в формате: '7XXXXXXXXXX', где X - цифра от 0 до 9"
                ]
            }
        }
    ),
}


mailing_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'name': openapi.Schema(type=openapi.TYPE_STRING, description=_('Имя'), example='Рассылка'),
        'start_datetime': openapi.Schema(
            type=openapi.TYPE_STRING, description=_('Дата начала рассылки'),
            example='02-12-2022 10:10:30', format='%d-%m-%Y %H:%M:%S'
        ),
        'text': openapi.Schema(type=openapi.TYPE_STRING, description=_('Текст рассылки'), example="Привет!"),
        'filter': openapi.Schema(
            type=openapi.TYPE_STRING, description=_('Фильтр по оператору или тегу'),
            example=_("999")
        ),
        'stop_datetime': openapi.Schema(
            type=openapi.TYPE_STRING, description=_('Дата конца рассылки'),
            example='02-12-2022 10:10:30', format='%d-%m-%Y %H:%M:%S'
        ),
    }
)

mailing_responses = {
    "201": openapi.Response(
        description="Создано",
        examples={
            "application/json": {
                "id": 52,
                "start_datetime": "02-12-2022 10:10:30",
                "stop_datetime": "03-12-2022 05:01:36",
                "name": "Рассылка",
                "text": "Привет!",
                "filter": "953"
            }
        }
    ),
    "400": openapi.Response(
        description="Ошибка"
    ),
}

mailing_ok_responses = {
    "200": openapi.Response(
        description="ОК",
        examples={
            "application/json": {
                "id": 52,
                "start_datetime": "02-12-2022 10:10:30",
                "stop_datetime": "03-12-2022 05:01:36",
                "name": "Рассылка",
                "text": "Привет!",
                "filter": "953"
            }
        }
    ),
    "400": openapi.Response(
        description="Ошибка"
    ),
}

get_overall_statistics_response = {
    "200": openapi.Response(
        description="ОК",
        examples={
            "application/json": {
                "Рассылка (id=52)": {
                    "Отправлено": 30,
                    "Не отправлено": 10
                },
                "Новая рассылка (id=53)": {
                    "Отправлено": 55,
                    "Не отправлено": 2
                }
            }
        }
    )
}

get_statistics_response = {
    "200": openapi.Response(
        description="ОК",
        examples={
            "application/json": {
                "Рассылка (id=40)": {
                    "Отправлено": 0,
                    "Не отправлено": 2
                },
                "Отправленные сообщения": [],
                "Не отправленные сообщения": [
                    {
                        "id сообщения": 38,
                        "id клиента": 1,
                        "телефон": "79999999999"
                    },
                    {
                        "id сообщения": 39,
                        "id клиента": 2,
                        "телефон": "79888888888"
                    }
                ]
            }
        }
    ),
    "404": openapi.Response(
        description="Не найдено"
    ),
}
